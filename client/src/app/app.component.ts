import { Component } from '@angular/core';
import { AppService } from './app-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Bark Park Online';

  public appService: AppService;

  constructor(appService: AppService) {
    this.appService = appService;
  }

}
