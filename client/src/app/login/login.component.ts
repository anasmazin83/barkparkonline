import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AppService } from '../app-service';
import { environment as env } from '../../environments/environment';
import { LoginCredentials } from './login-credentials'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public credentials: LoginCredentials;

  public appService: AppService;

  constructor(private http: HttpClient, appService: AppService) {
    this.appService = appService;
    this.credentials = new LoginCredentials();
  }

  ngOnInit(): void {
  }

  public keyPressed(event: any) {
    if (event.keyCode == 13) {
      this.appService.login(this.credentials);
    }
  }
}
