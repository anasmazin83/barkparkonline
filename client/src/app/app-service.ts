import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env } from '../environments/environment';
import { LoginCredentials } from './login/login-credentials';
import { UserProfile } from './user/user-profile';

@Injectable({
  providedIn: 'root',
})
export class AppService {

  public loggedIn: boolean = false;
  public loginFailed: boolean = false;
  public username: string = "";
  public profile: UserProfile;

  constructor(private http: HttpClient) { }

  public login(credentials : LoginCredentials) {
    const headers = new HttpHeaders(credentials ? {
      authorization : 'Basic ' + btoa(credentials.username + ":" + credentials.password)
    } : {});

    this.http.get(env.hostUrl + "user/login", {headers: headers}).subscribe(
      response => {
        this.profile = new UserProfile(response);
        this.loggedIn = true;
      },
      error => {
        this.loggedIn = false;
        this.loginFailed = true;
      }
    )

  }

}
