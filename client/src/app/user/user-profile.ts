import { URL } from "url";

export class UserProfile {
  public firstName: string;
  public lastName: string;
  public emailAddress: string;
  public phoneNumber: string;
  public profilePhotoUrl: string;

  public fullName: string;

  constructor(json: any) {
    this.firstName = json.firstName;
    this.lastName = json.lastName;
    this.emailAddress = json.emailAddress;
    this.phoneNumber = json.phoneNumber;
    this.profilePhotoUrl = json.profilePhotoUrl;

    this.fullName = this.firstName + " " + this.lastName;
  }
}
