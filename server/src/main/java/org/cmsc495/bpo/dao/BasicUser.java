package org.cmsc495.bpo.dao;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.cmsc495.bpo.dao.interfaces.User;

/**
 * State Object for a User. Validation constraints are added to User in order
 * for Spring to ensure that recieved User objects contain the required information.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BasicUser implements User {
    @NotNull
    @Valid
    private Credentials credentials;

    /**
     * User must have a valid profile
     */
    @NotNull
    @Valid
    private UserProfile userProfile;


    public BasicUser() {
    }

    public BasicUser(Credentials credentials, UserProfile userProfile) {
        this.credentials = credentials;
        this.userProfile = userProfile;
    }

    public Credentials getCredentials() {
        return this.credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public UserProfile getUserProfile() {
        return this.userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public BasicUser withCredentials(Credentials credentials) {
        this.credentials = credentials;
        return this;
    }

    public BasicUser withUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
        return this;
    }

    @Override
    public String toString() {
        return "Basic User [" + credentials.getUsername() + "]";
    }

    /**
     * For Sorting Users
     */
    @Override
    public int compareTo(User o) {
        if (o instanceof GuestUser) return 1;
        if (o instanceof Administrator) return -1;
        return this.getUsername().compareTo(o.getUsername());
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof BasicUser)) return false;
        BasicUser other = (BasicUser) o;
        return this.getUsername().equals(other.getUsername());
    }

    @Override
    public int hashCode() {
        return this.getUsername().hashCode();
    }

    @Override
    public String getUsername() {
        return this.credentials.getUsername();
    }

    @Override
    public Type getType() {
        return User.Type.BASIC;
    }
}
