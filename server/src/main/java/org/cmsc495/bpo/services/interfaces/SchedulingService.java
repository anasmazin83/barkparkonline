package org.cmsc495.bpo.services.interfaces;

import org.cmsc495.bpo.controllers.response.SchedulingMessage;
import org.cmsc495.bpo.dao.interfaces.User;

/**
 * Interface to define a Scheduling Service. The E type represents
 * the thing to be scheduled, while the K type represents the key
 * that corresponds to what is being scheduled.
 */
public interface SchedulingService<E, K> {
    /**
     * Add a scheduled object. The User should be checked to ensure
     * that they have the permissions or authority to perform
     * such a task.
     * 
     * @param user
     * @param o
     * @return
     */
    public SchedulingMessage add(User user, E o);

    /**
     * Remove a scheduled object based on its key. The User
     * should be checked to ensure that they have the permissions
     * or authority to perform such a task.
     * 
     * @param user
     * @param key
     * @return
     */
    public SchedulingMessage remove(User user, K key);

    /**
     * Edit a scheduled object based on the given key.
     * The User should be checked to ensure that they have
     * the permissions or authority to perform such a task.
     * 
     * @param user
     * @param key
     * @param o
     * @return
     */
    public SchedulingMessage edit(User user, K key, E o);

    /**
     * Retrieve the given scheduled object based on the 
     * provided key. This operation should be accessible to
     * everyone.
     * 
     * @param key
     * @return
     */
    public SchedulingMessage get(K key);
}
