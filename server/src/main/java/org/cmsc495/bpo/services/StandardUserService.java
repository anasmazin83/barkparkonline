package org.cmsc495.bpo.services;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.cmsc495.bpo.dao.GuestUser;
import org.cmsc495.bpo.dao.BasicUser;
import org.cmsc495.bpo.dao.UserProfile;
import org.cmsc495.bpo.dao.interfaces.User;
import org.cmsc495.bpo.exceptions.DuplicateUserException;
import org.cmsc495.bpo.exceptions.UserNotFoundException;
import org.cmsc495.bpo.repositories.BasicUserRepository;
import org.cmsc495.bpo.services.interfaces.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StandardUserService implements UserService {
    protected static final Logger log = LoggerFactory.getLogger(StandardUserService.class);

    private Map<String, User> users;

    private Set<User> currentUsers;

    private BasicUserRepository repo;

    @Autowired
    public StandardUserService(BasicUserRepository repo) {
        this.repo = repo;
        users = new HashMap<>();
        currentUsers = new HashSet<>();
    }

    protected StandardUserService(Map<String, User> users, BasicUserRepository repo) {
        this.users = users;
        this.repo = repo;
    }

    @Override
    public UserProfile getProfile(String username) throws UserNotFoundException {
        BasicUser user = repo.retrieve(username);
        try {
            user.getUserProfile().getFullName();
            return user.getUserProfile();

        } catch (NullPointerException npe) {}
        log.warn("USER [{}] does not have a profile or is not a USER", username);
        throw new UserNotFoundException(username);
    }

    @Override
    public User getUser(String username) throws UserNotFoundException {
        User user = repo.retrieve(username);
        if (user == null) throw new UserNotFoundException(username);
        return user;
    }

    @Override
    public void addUser(User user) throws DuplicateUserException {
        if (!(user instanceof BasicUser)) {
            log.warn("USER not added. Currently, there is no repository support for {} USERS.", user.getType());
            return;
        }
        try {
            userIsValid(user);
            repo.create((BasicUser) user);
            log.info("{} USER [{}] created", user.getType(), user.getUsername());

        } catch (IllegalArgumentException e) { // Duplicate user
            throw new DuplicateUserException(user.getUsername());

        } catch (ValidationException ve) {
            throw ve;
        }   
    }

    @Override
    public boolean removeUser(User user) {
        return this.users.remove(user.getCredentials().getUsername()).equals(user);
    }

    @Override
    public void login(User user) {
        final String HAS_LOGGED_IN = "has logged in";
        final String WAS_ALREADY_LOGGED_IN = "was already logged in";
        String msg = WAS_ALREADY_LOGGED_IN;

        if (this.currentUsers.add(user)) msg = HAS_LOGGED_IN;

        if (user instanceof GuestUser) {
            log.info("{} USER [{}] {}", user.getType(), user.getUsername(), msg);
        }
        else { 
            log.info("{} USER [{}] {}", user.getType(), user.getUsername(), msg); 
        }
    }

    @Override
    public Set<User> getCurrentUsers() {
        return this.currentUsers;
    }

    /** Stubbing */
    protected void userIsValid(User user) { UserValidator.validate(user); }

    /**
     * Validator for validating a BasicUser
     */
    protected static class UserValidator {
        protected static void validate(User user) throws ValidationException {
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            Validator validator = factory.getValidator();
            Set<ConstraintViolation<User>> violations = validator.validate(user);
            for (ConstraintViolation<User> violation : violations) {
                log.warn("{} USER [{}] : {}", user.getType(), user.getUsername(), violation.getMessage());
            }
            if (violations.size() > 0) 
                throw new ValidationException(user.getType() + 
                " USER [" + user.getUsername() + "] had " + 
                violations.size() + " invalid fields");
        }
    }
}
