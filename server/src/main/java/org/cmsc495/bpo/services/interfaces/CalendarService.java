package org.cmsc495.bpo.services.interfaces;

import org.cmsc495.bpo.dao.interfaces.CalendarModel;

public interface CalendarService {
    public static int MAX_YEAR = 2021;
    public static int MIN_YEAR = 2020;
    public static int MIN_MONTH = 1;
    public static int MAX_MONTH = 12;

    /**
     * Retrive a Calendar Model for the given Year and Month.
     * 
     * @param year
     * @param month
     * @return
     */
	CalendarModel getModel(int year, int month);
}
