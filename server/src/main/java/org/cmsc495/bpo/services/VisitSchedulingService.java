package org.cmsc495.bpo.services;

import org.cmsc495.bpo.controllers.response.SchedulingMessage;
import org.cmsc495.bpo.dao.Visit;
import org.cmsc495.bpo.dao.interfaces.User;
import org.cmsc495.bpo.security.Permissions;
import org.cmsc495.bpo.services.interfaces.CalendarService;
import org.cmsc495.bpo.services.interfaces.SchedulingService;
import org.cmsc495.bpo.services.interfaces.UserService;
import org.cmsc495.bpo.util.StringUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VisitSchedulingService implements SchedulingService<Visit, String> {
	protected static final Logger log = LoggerFactory.getLogger(VisitSchedulingService.class);
	
	private final String USER_DOES_NOT_HAVE_PERMISSION_TO = "User [{}] does not have {} permission";
	private final String USER_HAS_PERFORMED_VISIT_ACTION = "User [{}] has {} a Visit";
	private final String VISIT_RETRIEVED = "Visit [{}] retrieved";

	private UserService userService;
	private CalendarService calendarService;

	@Autowired
	public VisitSchedulingService
	(
		UserService userService,
		CalendarService calendarService
	) {
		this.userService = userService;
		this.calendarService = calendarService;
	}

	@Override
	public SchedulingMessage add(User user, Visit o) {
		if (!userHasCreatePermissions(user)) {
			log.info(USER_DOES_NOT_HAVE_PERMISSION_TO, user, Permissions.CREATE);

			return schedulingMessage(
				false, 
				debrace(USER_DOES_NOT_HAVE_PERMISSION_TO, user, Permissions.CREATE)
			);
		}

		// TODO: Implement business logic here

		log.info(USER_HAS_PERFORMED_VISIT_ACTION, user, "created");

		return schedulingMessage(
			true,
			debrace(USER_HAS_PERFORMED_VISIT_ACTION, user, "created")
		);
	}

	@Override
	public SchedulingMessage remove(User user, String key) {
		if (!userHasRemovePermissions(user)) {
			log.info(USER_DOES_NOT_HAVE_PERMISSION_TO, user, Permissions.REMOVE);

			return schedulingMessage(
				false, 
				debrace(USER_DOES_NOT_HAVE_PERMISSION_TO, user, Permissions.REMOVE)
			);
		}

		// TODO: Implement business logic here

		log.info(USER_HAS_PERFORMED_VISIT_ACTION, user, "removed");

		return schedulingMessage(
			true,
			debrace(USER_HAS_PERFORMED_VISIT_ACTION, user, "removed")
		);
	}

	@Override
	public SchedulingMessage edit(User user, String key, Visit o) {
		if (!userHasEditPermissions(user)) {
			log.info(USER_DOES_NOT_HAVE_PERMISSION_TO, user, Permissions.EDIT);

			return schedulingMessage(
				false, 
				debrace(USER_DOES_NOT_HAVE_PERMISSION_TO, user, Permissions.EDIT)
			);
		}

		// TODO: Implement business logic here

		log.info(USER_HAS_PERFORMED_VISIT_ACTION, user, "edited");

		return schedulingMessage(
			true,
			debrace(USER_HAS_PERFORMED_VISIT_ACTION, user, "edited")
		);
	}

	@Override
	public SchedulingMessage get(String key) {

		// TODO: Implement business logic here

        log.info(VISIT_RETRIEVED, key);
		return schedulingMessage(true, debrace(VISIT_RETRIEVED, key));
	}

	/** Stubbing */
	protected UserService getUserService() { return this.userService; }
	protected CalendarService getCalendarService() { return this.calendarService; }
	protected String debrace(String msg, Object... o) { return StringUtility.debrace(msg, o); }
	protected boolean userHasCreatePermissions(User user) { return User.hasPermissions(user, Permissions.CREATE); }
	protected boolean userHasRemovePermissions(User user) { return User.hasPermissions(user, Permissions.REMOVE); }
	protected boolean userHasEditPermissions(User user) { return User.hasPermissions(user, Permissions.EDIT); }
	protected SchedulingMessage schedulingMessage(boolean accepted, Object msg) { return new SchedulingMessage(accepted, msg); }
}
