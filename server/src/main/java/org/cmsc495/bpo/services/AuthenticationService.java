package org.cmsc495.bpo.services;

import org.cmsc495.bpo.dao.Credentials;
import org.cmsc495.bpo.repositories.BasicUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService implements AuthenticationProvider {
    protected static final Logger log = LoggerFactory.getLogger(AuthenticationService.class);
    private BasicUserRepository repo;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public AuthenticationService(BasicUserRepository repo, PasswordEncoder passwordEncoder) {
        this.repo = repo;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        final String USERNAME = authentication.getName();
        final String PASSWORD = authentication.getCredentials().toString();

        try {
            Credentials cred = repo.retrieve(USERNAME).getCredentials();
            if (passwordEncoder.matches(PASSWORD, cred.getPassword())) {
                return authentication;
            } 
            else {
                log.warn("USER [{}] could not be authenticated, Reason: {}", USERNAME, "Incorrect Password");
            }
        }
        catch (NullPointerException npe) {
            log.warn("USER [{}] could not be authenticated, Reason: User Not Found", USERNAME);
        }
        catch (Exception e) {
            log.warn("USER [{}] could not be authenticated, Reason: {}", USERNAME, e.getLocalizedMessage());
        }
        throw new AuthenticationServiceException("Authentication failed for User " + USERNAME);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
