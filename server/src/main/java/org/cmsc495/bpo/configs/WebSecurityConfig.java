package org.cmsc495.bpo.configs;

import org.cmsc495.bpo.services.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SuppressWarnings("all")
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    protected static final Logger log = LoggerFactory.getLogger(WebSecurityConfig.class);

    private AuthenticationService authenticationService;

    @Autowired
    public WebSecurityConfig(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

	@Override
	protected void configure(HttpSecurity http) throws Exception {
        http
            .cors()
                .and()
            .authorizeRequests()
                // Allow home page and calendar to be reached without 
                // requiring Users to be authenticated
                .antMatchers("/", "/home", "/calendar/**", "/user/signup").permitAll()
                .and()
            .authorizeRequests()
                // Require Users to authenticate for User-related actions
                .antMatchers("/user/**").authenticated()
                .anyRequest().permitAll()
                .and()
            .httpBasic()   
                .and()
            // TODO: Disable this and setup CSRF Token in Angular
            .csrf().disable();
            log.info("Web Security Configuration Loaded");
    }
    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationService).eraseCredentials(false);
    }
}