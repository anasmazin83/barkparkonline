package org.cmsc495.bpo.controllers;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.cmsc495.bpo.dao.BasicUser;
import org.cmsc495.bpo.dao.GuestUser;
import org.cmsc495.bpo.dao.UserProfile;
import org.cmsc495.bpo.dao.interfaces.User;
import org.cmsc495.bpo.exceptions.DuplicateUserException;
import org.cmsc495.bpo.exceptions.UserNotFoundException;
import org.cmsc495.bpo.services.interfaces.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Here, we define a REST Controller that handles all HTTP requests related to
 * an End User
 */
@Controller()
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {
    protected static final Logger log = LoggerFactory.getLogger(UserController.class);

    private UserService userService;

    /**
     * Autowire tells Spring Boot to find and automatically import the required
     * class implementations in order to construct this class. For example, Spring
     * Boot will find our implementation of UserService and autowire it to this
     * instance.
     */
    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * This endpoint returns a User's Profile when they login.
     * You do not need to be authenticated to reach this endpoint.
     * However, if you are not an authenticated User you will be
     * treated as a guest.
     */
    @GetMapping(path = "/user/login")
    public ResponseEntity<?> userLogin(Principal principal, HttpServletRequest request) {
        String username = principal.getName();

        User user = new GuestUser(request.getRemoteAddr());
        Object body = new UserProfile();

        // Try to get the profile for the given user, then return it
        try {
            user = userService.getUser(username);
            body = userService.getProfile(username);

        // Catch User Not Found Exception and log the error
        } catch (UserNotFoundException enfe) {
            enfe.printStackTrace();
            log.warn(enfe.getLocalizedMessage());
            log.info("USER {} will be logged in as a GUEST instead", username);
        }
        userService.login(user);

        // Return a response to the Client
        return ResponseEntity.ok().body(body);
    }

    /**
     * This endpoint accepts a '@Valid' User only. The User is then added to the current 
     * implementation of UserService.
     * 
     * @param user
     * @return
     */
    @PostMapping(path = "/user/signup")
    public ResponseEntity<?> signUp(@Valid @RequestBody(required = true) BasicUser user) {

        // Try to add the given User to the User Service
        try {
            userService.addUser(user);
            return ResponseEntity.ok().build();

        // An exception may be thrown if there is a database error
        } catch (DuplicateUserException ex) {
            log.error(ex.getLocalizedMessage());
            return ResponseEntity.status(HttpStatus.CONFLICT).body(ex.getMessage());

        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
        }

        return ResponseEntity
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body("Unknown error occurred. User " + user.getCredentials().getUsername() + " was not signed up");
    }
}
