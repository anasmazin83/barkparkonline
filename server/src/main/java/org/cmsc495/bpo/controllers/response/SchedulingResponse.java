package org.cmsc495.bpo.controllers.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * ResponseEntity specifically built for Helpful Scheduling Responses
 */
public class SchedulingResponse extends ResponseEntity<SchedulingMessage> {
    private SchedulingMessage message;

    private SchedulingResponse(HttpStatus status) {
        super(status);
    }

    public SchedulingResponse(HttpStatus status, SchedulingMessage message) {
        super(status);
        this.message = message;
    }

    @Override
    public SchedulingMessage getBody() {
        return message;
    }
}
