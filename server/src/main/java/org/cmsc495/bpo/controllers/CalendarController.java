package org.cmsc495.bpo.controllers;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.cmsc495.bpo.controllers.response.SchedulingMessage;
import org.cmsc495.bpo.controllers.response.SchedulingResponse;
import org.cmsc495.bpo.dao.GuestUser;
import org.cmsc495.bpo.dao.Visit;
import org.cmsc495.bpo.dao.interfaces.CalendarModel;
import org.cmsc495.bpo.dao.interfaces.User;
import org.cmsc495.bpo.services.interfaces.CalendarService;
import org.cmsc495.bpo.services.interfaces.SchedulingService;
import org.cmsc495.bpo.services.interfaces.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Here, we define a REST Controller to handle all requests related
 * to the Calendar to include: User Visits, Calendar Data, Park Info, etc.
 * 
 * <p>Because the Calendar is open to the public, these endpoints do not require
 * authentication in order to be reached.</p>
 */
@Controller
@CrossOrigin(origins = "http://localhost:4200")
public class CalendarController {
    protected static final Logger log = LoggerFactory.getLogger(CalendarController.class);

    private CalendarService calendarService;
    private SchedulingService<Visit, String> schedulingService;
    private UserService userService;
    
    @Autowired
    public CalendarController
    (
        CalendarService calendarService, 
        SchedulingService<Visit, String> schedulingService,
        UserService userService
    ) {
        this.calendarService = calendarService;
        this.schedulingService = schedulingService;
        this.userService = userService;
    }

    /**
     * Fetches the Calendar Model from the CalendarService and returns it in
     * the Response Body for the requester.
     */
    @GetMapping(path = "/calendar/{year}/{month}")
    @ResponseBody
    public ResponseEntity<?> getCalendarModel
    (
        @PathVariable(value = "year") Integer year,
        @PathVariable(value = "month") Integer month
    ) {
        CalendarModel model = calendarService.getModel(year, month);
        if (model != null) {
            return ResponseEntity.ok().body(model);
        }
        return ResponseEntity.badRequest().body("Calendar Not Found");
    }
    
    /**
     * (Attempt to) Create a Visit and return a Scheduling Message to the requester.
     */
    @PostMapping(path = "/calendar/scheduling/visit/create")
    @ResponseBody
    public ResponseEntity<?> createVisit
    (
        @Valid Visit visit,
        Principal principal,
        HttpServletRequest request
    ) {
        User user = newGuest(request.getRemoteAddr()); // User is guest by default
        try {
			user = userService.getUser(principal.getName());
        } catch (Exception e) { 
            // Do Nothing, leave as Guest
        }
        // Always return OK status so it's easier to handle on client-side
        return newSchedulingResponse(schedulingService.add(user, visit));
    }

    /**
     * (Attempt to) Edit a Visit specified by the visitId, then return a 
     * Scheduling Message back to the requester.
     */
    @PutMapping(path = "/calendar/scheduling/visit/edit/{visitId}")
    @ResponseBody
    public ResponseEntity<?> editVisit
    (
        @PathVariable(value = "visitId") String visitId,
        @Valid Visit visit,
        Principal principal,
        HttpServletRequest request
    ) {
        User user = newGuest(request.getRemoteAddr()); // User is guest by default
        try {
			user = userService.getUser(principal.getName());
        } catch (Exception e) { 
            // Do Nothing 
        }
        // Always return OK status so it's easier to handle on client-side
        return newSchedulingResponse(schedulingService.edit(user, visitId, visit));
    }

    /**
     * (Attempt to) Remove a Visit specified by the given Visit ID,
     * then return a Scheduling Message to the requester.
     */
    @DeleteMapping(path = "/calendar/scheduling/visit/remove/{visitId}")
    @ResponseBody
    public ResponseEntity<?> removeVisit
    (
        @PathVariable(value = "visitId") String visitId,
        Principal principal,
        HttpServletRequest request
    ) {
        User user = newGuest(request.getRemoteAddr()); // User is guest by default
        try {
			user = userService.getUser(principal.getName());
        } catch (Exception e) { 
            // Do Nothing 
        }
        // Always return OK status so it's easier to handle on client-side
        return newSchedulingResponse(schedulingService.remove(user, visitId));
    }

    /**
     * (Attempt to) Retrieve a Visit specified by the given Visit ID
     * if the Visit exists.
     */
    @GetMapping(path = "/calendar/scheduling/visit/{visitId}")
    @ResponseBody
    public ResponseEntity<?> getVisit(@PathVariable(value = "visitId") String visitId) {
        return newSchedulingResponse(schedulingService.get(visitId));
    }

    /** Stubbing */
    protected GuestUser newGuest(String ip) { return new GuestUser(ip); }
    protected SchedulingResponse newSchedulingResponse(SchedulingMessage message) {
        // Always return OK status so it's easier to handle on client-side
        return new SchedulingResponse(HttpStatus.OK, message);
    }
}
