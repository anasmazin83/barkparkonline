Feature: Test that a Registered User can Login

  Scenario: As a user, I want to login
    Given the web server is online
    And i can access the login page
    When i type in my username
    And i type in my password
    And i press the login button
    Then i am logged into the server