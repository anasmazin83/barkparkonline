package org.cmsc495.bpo.cucumber.config;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * This is the driver for running End to End (E2E) tests with Cucumber.
 * 
 * All Feature files from the resources directory will be loaded and tested
 * against the available Step Definitions within CucumberStepDefinitions.java
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "json:target/cucumber.json"},
        features = "src/test/resources",
        glue = {"org.cmsc495.bpo.cucumber.glue"}
)
public class CucumberIntegrationTestConfig extends SpringIntegrationTestConfig {
}