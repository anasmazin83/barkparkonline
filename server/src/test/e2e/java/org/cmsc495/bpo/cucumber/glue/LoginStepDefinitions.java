package org.cmsc495.bpo.cucumber.glue;

import org.springframework.web.client.RestTemplate;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.cmsc495.bpo.controllers.response.ServerStatus;
import org.cmsc495.bpo.controllers.response.ServerStatus.Status;
import org.cmsc495.bpo.cucumber.config.SpringIntegrationTestConfig;
import org.cmsc495.bpo.cucumber.drivers.FirefoxSeleniumDriver;
import org.cmsc495.bpo.cucumber.webpages.LoginPage;
import org.cmsc495.bpo.dao.BasicUser;
import org.cmsc495.bpo.dao.Credentials;
import org.cmsc495.bpo.services.interfaces.UserService;

/**
 * All Integration Step Definitions for Cucumber should be placed here to ensure
 * that there is no repitition of work. The Spring Application will automatically 
 * load so that a real context can be used for tests.
 */
@CucumberContextConfiguration
public class LoginStepDefinitions extends SpringIntegrationTestConfig {
    protected static final Logger log = LoggerFactory.getLogger(LoginStepDefinitions.class);

    protected FirefoxSeleniumDriver firefox;

    /** Basic User for Testing */
    protected final String JON_SNOW_USERNAME = "jonsnow";
    protected final String PASSWORD = "ghost4ever";
    protected final Credentials CREDENTIALS = new Credentials()
        .withUsername(JON_SNOW_USERNAME)
        .withPassword(PASSWORD);
    protected final BasicUser JON_SNOW = new BasicUser()
        .withCredentials(CREDENTIALS);

    @Autowired
    protected UserService userService;

    @Before
    public void init() {
        serverUrl = "http://localhost:" + port;
        restTemplate = new RestTemplate();
        
        // Here, we integrate the Selenium framework to test against
        // the actual client app using Firefox 
        firefox = new FirefoxSeleniumDriver(serverUrl + "/");
    }

    @Given("^the web server is online$")
    public void webServerOnline() {
        ResponseEntity<ServerStatus> response = restTemplate.getForEntity(serverUrl  + "/" + "status" , ServerStatus.class);
        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertEquals(Status.UP_AND_RUNNING, response.getBody().getStatus());
    }

    @Given("^i can access the login page$")
    public void loginPageAccessible() throws Exception {
        LoginPage loginPage = LoginPage.init();
        firefox.goTo(loginPage);

        assertEquals(loginPage.getUrl(), firefox.getCurrentUrl());
    }

    @When("^i type in my username$")
    public void typeInUsername() {
        LoginPage loginPage = LoginPage.init();
        firefox.goTo(loginPage);
        loginPage.inputUsername(JON_SNOW_USERNAME);

        assertEquals(
            JON_SNOW_USERNAME, 
            loginPage.getUsernameInput().getAttribute("value")
        );
        sleepFor(1);
    }

    @When("^i type in my password$")
    public void typeInPassword() {
        LoginPage loginPage = LoginPage.init();
        firefox.goTo(loginPage);
        loginPage.inputPassword(PASSWORD);

        assertEquals(
            PASSWORD, 
            loginPage.getPasswordInput().getAttribute("value")
        );
        sleepFor(1);
    }

    @When("^i press the login button$")
    public void pressLogin() {
        LoginPage loginPage = LoginPage.init();
        firefox.goTo(loginPage);

        loginPage.clickLoginButton();
        sleepFor(1);
    }

    @Then("^i am logged into the server$")
    public void userLoggedIn() {
        boolean loggedIn = userService.getCurrentUsers().contains(JON_SNOW);
        if (!loggedIn) {  
            sleepFor(2); // in case of race condition
            loggedIn = userService.getCurrentUsers().contains(JON_SNOW);
        }
        assertTrue(loggedIn);
    }

    @After
    public void shutdown() {
        firefox.closeWindow();
    }
}
