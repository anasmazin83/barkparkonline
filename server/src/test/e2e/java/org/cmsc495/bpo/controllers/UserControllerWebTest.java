package org.cmsc495.bpo.controllers;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mongodb.client.result.DeleteResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.cmsc495.bpo.dao.Credentials;
import org.cmsc495.bpo.dao.DogProfile;
import org.cmsc495.bpo.Application;
import org.cmsc495.bpo.dao.BasicUser;
import org.cmsc495.bpo.dao.UserProfile;
import org.cmsc495.bpo.repositories.BasicUserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.result.StatusResultMatchers;

/**
 * Test with: gradle test --tests UserControllerWebTest
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class UserControllerWebTest {

    protected static final Logger log = LoggerFactory.getLogger(UserControllerWebTest.class);

    private final static String USERNAME = "powpowpow";

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private BasicUserRepository userRepo;

    StatusResultMatchers status = MockMvcResultMatchers.status();

    private static BasicUser getDummyUser() {
        Set<DogProfile> dogs = new HashSet<DogProfile>();
            dogs.add(new DogProfile(
                "Barky",
                "German Shephard",
                LocalDate.of(2018, 1, 1),
                DogProfile.Gender.MALE
            ));
        UserProfile profile = new UserProfile(
            "thisthis", 
            "fakestring", 
            "123-456-7890", 
            dogs, 
            "http://localhost:8080/file.png"
        );
        Credentials credentials = new Credentials("tenpowpowpow", USERNAME, "thispow@example.com", "lastone");
        credentials.setPassword("secretpassword");
        BasicUser user = new BasicUser();
        user.setUserProfile(profile);
        user.setCredentials(credentials);
        return user;
    }

    private ObjectWriter getWriter() {
        DefaultPrettyPrinter.Indenter indenter = new DefaultIndenter("  ", DefaultIndenter.SYS_LF);
        DefaultPrettyPrinter printer = new DefaultPrettyPrinter();
        printer.indentObjectsWith(indenter);
        printer.indentArraysWith(indenter);
        return mapper.writer(printer);
    }

    private String toJSON(Object o) throws JsonProcessingException {
        return getWriter().writeValueAsString(o);
    }

    private MockHttpServletRequestBuilder post(String url) {
        return MockMvcRequestBuilders.post(url).contentType(MediaType.APPLICATION_JSON);
    }

    @Before
    public void init() {
        DeleteResult result = userRepo.delete(USERNAME);
        log.info("BEFORE {}", result.toString());
    }

    @Test
    public void testSignUp_httpValidUser() throws Exception {
        BasicUser user = getDummyUser();
        String url = "/user/signup";
        String json = toJSON(user);

        log.info("POST {} {}", url, json);
        MvcResult res = mvc.perform(post(url).content(json)).andExpect(status.isOk()).andReturn();
        String body = res.getResponse().getContentAsString();
        log.info("USER {}", body);
    }

    @Test
    public void duplicateUserSignup() throws Exception {

        BasicUser user = getDummyUser();

        String url = "/user/signup";
        String json = toJSON(user);

        log.info("POST {} {}", url, json);
        mvc.perform(post(url).content(json)).andExpect(status.isOk());
        mvc.perform(post(url).content(json)).andExpect(status.isConflict());
    }

    @After
    public void cleanup() {
        DeleteResult result = userRepo.delete(USERNAME);
        log.info("AFTER {}", result.toString());
    }
}
