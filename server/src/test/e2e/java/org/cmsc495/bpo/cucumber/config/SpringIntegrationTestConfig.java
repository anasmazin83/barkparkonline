package org.cmsc495.bpo.cucumber.config;

import org.cmsc495.bpo.Application;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

import io.cucumber.spring.CucumberContextConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class SpringIntegrationTestConfig { 
        @LocalServerPort
        protected int port;
        protected RestTemplate restTemplate;
        protected String serverUrl; 

        /**
         * Sleeps the current thread for X seconds. Useful for e2e tests
         * when watching the Browser.
         * 
         * @param seconds
         */
        protected void sleepFor(int seconds) {
                try 
                {
                        Thread.sleep(seconds * 1000);
                }
                catch (Exception e) {
                }
        }
}
