package org.cmsc495.bpo.services;

import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

import org.cmsc495.bpo.dao.BasicUser;
import org.cmsc495.bpo.dao.Credentials;
import org.cmsc495.bpo.repositories.BasicUserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;

public class AuthenticationServiceTest {
    private AuthenticationService auth;

    @Mock
    private BasicUserRepository repo;

    @Mock
    private PasswordEncoder encoder;

    @Before
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        auth = new AuthenticationService(repo, encoder);
    }

    @Test
    public void testAuthenticate() throws Exception {
        Authentication authentication = mock(Authentication.class);
        Credentials cred = mock(Credentials.class);
        BasicUser user = mock(BasicUser.class);

        String username = "Bob Marley";
        String rawPassword = "d0ntw0rry!";
        String encodedPassword = "ab0ut@th!ng";

        doReturn(username).when(authentication).getName();
        doReturn(rawPassword).when(authentication).getCredentials();
        doReturn(encodedPassword).when(cred).getPassword();
        doReturn(cred).when(user).getCredentials();
        doReturn(user).when(repo).retrieve(username);
        doReturn(true).when(encoder).matches(rawPassword, encodedPassword);

        assertEquals(authentication, auth.authenticate(authentication));
    }

    @Test
    public void testAuthenticate_butPasswordsDontMatch() throws Exception {
        Authentication authentication = mock(Authentication.class);
        BasicUser user = mock(BasicUser.class);
        Credentials cred = mock(Credentials.class);

        String username = "Bob Marley";
        String rawPassword = "d0ntw0rry!";
        String encodedPassword = "ab0ut@th!ng";

        doReturn(username).when(authentication).getName();
        doReturn(rawPassword).when(authentication).getCredentials();
        doReturn(encodedPassword).when(cred).getPassword();
        doReturn(cred).when(user).getCredentials();
        doReturn(user).when(repo).retrieve(username);
        doReturn(false).when(encoder).matches(rawPassword, encodedPassword);

        assertThrows(AuthenticationServiceException.class, () -> {
            auth.authenticate(authentication);
        });
    }

    @Test
    public void testAuthenticate_butUserDoesntExist() throws Exception {
        Authentication authentication = mock(Authentication.class);
        Credentials cred = mock(Credentials.class);

        String username = "Bob Marley";
        String rawPassword = "d0ntw0rry!";
        String encodedPassword = "ab0ut@th!ng";

        doReturn(username).when(authentication).getName();
        doReturn(rawPassword).when(authentication).getCredentials();
        doReturn(encodedPassword).when(cred).getPassword();

        doThrow(NullPointerException.class).when(repo).retrieve(username);
        doReturn(false).when(encoder).matches(rawPassword, encodedPassword);

        assertThrows(AuthenticationServiceException.class, () -> {
            auth.authenticate(authentication);
        });
    }

    @Test
    public void testSupports() throws Exception {
        Class<?> authentication = UsernamePasswordAuthenticationToken.class;
        assertTrue(auth.supports(authentication));
    }
}
