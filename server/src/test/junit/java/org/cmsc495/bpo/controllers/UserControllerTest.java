package org.cmsc495.bpo.controllers;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.cmsc495.bpo.dao.Credentials;
import org.cmsc495.bpo.dao.BasicUser;
import org.cmsc495.bpo.dao.UserProfile;
import org.cmsc495.bpo.exceptions.UserNotFoundException;
import org.cmsc495.bpo.services.interfaces.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

public class UserControllerTest {
    protected static final Logger log = LoggerFactory.getLogger(UserController.class);
    private UserController controller;

    @Mock
    private UserService userService;

    @Before
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        controller = new UserController(userService);
    }

    @Test
    public void testUserLogin() throws Exception {
        String userName = "woody";
        UserProfile woodysProfile = mock(UserProfile.class);
        Principal principal = mock(Principal.class);
        HttpServletRequest request = mock(HttpServletRequest.class);

        doReturn(userName).when(principal).getName();
        doReturn(woodysProfile).when(userService).getProfile(userName);

        ResponseEntity<?> response = controller.userLogin(principal, request);
        assertTrue(response.getStatusCode().is2xxSuccessful());
    }

    @Test
    public void testUserLogin_butUserIsNotFound() throws Exception {
        String userName = "mikewazowski (guest)";
        Principal principal = mock(Principal.class); 
        HttpServletRequest request = mock(HttpServletRequest.class);

        doReturn(userName).when(principal).getName();
        doThrow(new UserNotFoundException("Not found")).when(userService).getProfile(userName);

        ResponseEntity<?> response = controller.userLogin(principal, request);
        assertTrue(response.getStatusCode().is2xxSuccessful());
        assertTrue(response.getBody() instanceof UserProfile);
        UserProfile profile = (UserProfile) response.getBody();
        assertTrue(profile.getFullName().contains("null")); // Guest does not have a full name
    }

    @Test
    public void testSignUp() throws Exception {
        BasicUser austinPowers = mock(BasicUser.class); 
        ResponseEntity<?> response = controller.signUp(austinPowers);
        assertTrue(response.getStatusCode().is2xxSuccessful());
        verify(userService, times(1)).addUser(austinPowers);
    }

    @Test
    public void testSignUp_butUnknownExceptionWasThrown() throws Exception {
        BasicUser goldmember = mock(BasicUser.class);
        Credentials austinPowers = mock(Credentials.class);
        String username = "AustinPowWow";
        doReturn(username).when(austinPowers).getUsername();
        doReturn(austinPowers).when(goldmember).getCredentials();
        doThrow(NullPointerException.class).when(userService).addUser(goldmember);
        ResponseEntity<?> response = controller.signUp(goldmember);
        assertTrue(response.getStatusCode().is5xxServerError());
    }
}
