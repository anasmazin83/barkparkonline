package org.cmsc495.bpo.controllers;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.cmsc495.bpo.controllers.response.SchedulingMessage;
import org.cmsc495.bpo.controllers.response.SchedulingResponse;
import org.cmsc495.bpo.dao.BasicUser;
import org.cmsc495.bpo.dao.GuestUser;
import org.cmsc495.bpo.dao.Visit;
import org.cmsc495.bpo.dao.interfaces.CalendarModel;
import org.cmsc495.bpo.exceptions.UserNotFoundException;
import org.cmsc495.bpo.services.interfaces.CalendarService;
import org.cmsc495.bpo.services.interfaces.SchedulingService;
import org.cmsc495.bpo.services.interfaces.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

public class CalendarControllerTest {
    private CalendarController controller;

    @Mock
    private CalendarService calendarService;

    @Mock
    private SchedulingService<Visit, String> schedulingService;

    @Mock
    private UserService userService;

    @Mock
    private GuestUser guest;

    @Mock
    private SchedulingResponse schedulingResponse;

    @Mock SchedulingMessage message;

    @Before
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        controller = spy(new CalendarController(calendarService, schedulingService, userService));

        doReturn(guest).when(controller).newGuest(any());
        doReturn(schedulingResponse).when(controller).newSchedulingResponse(any());
        doReturn(message).when(schedulingResponse).getBody();
    }

    @Test
    public void testGetCalendarModel() throws Exception {
        int year = 2020;
        int month = 4;
        CalendarModel model = mock(CalendarModel.class);

        doReturn(model).when(calendarService).getModel(year, month);

        ResponseEntity<?> response = controller.getCalendarModel(year, month);
        assertEquals(model, response.getBody());
    }

    @Test
    public void testGetCalendarModel_butModelIsNull() throws Exception {
        doReturn(null).when(calendarService).getModel(1, 2);
        ResponseEntity<?> response = controller.getCalendarModel(1, 2);
        assertTrue(response.getStatusCode().is4xxClientError());
    }

    @Test
    public void testCreateVisit() throws Exception {
        Visit visit = mock(Visit.class);
        Principal principal = mock(Principal.class);
        BasicUser chrisTucker = mock(BasicUser.class);
        String username = "Chris Tucker";
        HttpServletRequest request = mock(HttpServletRequest.class);

        doReturn("192.168.0.1").when(request).getRemoteAddr();
        doReturn(username).when(principal).getName();
        doReturn(chrisTucker).when(userService).getUser(username);
        
        ResponseEntity<?> response = controller.createVisit(visit, principal, request);
        assertTrue(response instanceof SchedulingResponse);
        verify(schedulingService, times(1)).add(chrisTucker, visit);
    }

    @Test
    public void testVisitEndpointsForGuests() throws Exception {
        String visitId = "123456789";
        Visit visit = mock(Visit.class);
        Principal principal = mock(Principal.class);
        String username = "i-dont-exist";
        HttpServletRequest request = mock(HttpServletRequest.class);

        doReturn("192.168.0.1").when(request).getRemoteAddr();
        doReturn(username).when(principal).getName();

        // Since User is Not found, we will always pass on the Guest User
        doThrow(UserNotFoundException.class).when(userService).getUser(username);

        // Create Visit
        controller.createVisit(visit, principal, request);
        verify(schedulingService, times(1)).add(guest, visit);

        // Edit Visit
        controller.editVisit(visitId, visit, principal, request);
        verify(schedulingService, times(1)).edit(guest, visitId, visit);

        // Remove Visit
        controller.removeVisit(visitId, principal, request);
        verify(schedulingService, times(1)).remove(guest, visitId);
    }

    @Test
    public void testEditVisit() throws Exception {
        String visitId = "Kung Fu";
        Visit visit = mock(Visit.class);
        Principal principal = mock(Principal.class);
        BasicUser jackieChan = mock(BasicUser.class);
        String username = "Jackie Chan";
        HttpServletRequest request = mock(HttpServletRequest.class);

        doReturn("192.168.0.1").when(request).getRemoteAddr();
        doReturn(username).when(principal).getName();
        doReturn(jackieChan).when(userService).getUser(username);
        
        ResponseEntity<?> response = controller.editVisit(visitId, visit, principal, request);
        assertTrue(response instanceof SchedulingResponse);
        verify(schedulingService, times(1)).edit(jackieChan, visitId, visit);
    }

    @Test
    public void testRemoveVisit() throws Exception {
        String visitId = "Mission Not Impossible";
        Principal principal = mock(Principal.class);
        BasicUser tomCruise = mock(BasicUser.class);
        String username = "Tom Cruise";
        HttpServletRequest request = mock(HttpServletRequest.class);

        doReturn("192.168.0.1").when(request).getRemoteAddr();
        doReturn(username).when(principal).getName();
        doReturn(tomCruise).when(userService).getUser(username);
        
        ResponseEntity<?> response = controller.removeVisit(visitId, principal, request);
        assertTrue(response instanceof SchedulingResponse);
        verify(schedulingService, times(1)).remove(tomCruise, visitId);
    }

    @Test
    public void testGetVisit() throws Exception {
        String visitId = "Mission Not Impossible";
        
        ResponseEntity<?> response = controller.getVisit(visitId);
        assertTrue(response instanceof SchedulingResponse);
        verify(schedulingService, times(1)).get(visitId);
    }

    @Test 
    public void isThisThingOn() {
        
    }
}
