package org.cmsc495.bpo.services;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.cmsc495.bpo.controllers.response.SchedulingMessage;
import org.cmsc495.bpo.dao.Administrator;
import org.cmsc495.bpo.dao.GuestUser;
import org.cmsc495.bpo.dao.Visit;
import org.cmsc495.bpo.dao.BasicUser;
import org.cmsc495.bpo.services.interfaces.CalendarService;
import org.cmsc495.bpo.services.interfaces.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class VisitSchedulingServiceTest {
    private VisitSchedulingService service;

    private final String KEY = "O-:;";

    @Mock
    private UserService userService;

    @Mock
    private CalendarService calendarService;

    @Mock
    private BasicUser basicUser;

    @Mock
    private Administrator admin;

    @Mock
    private GuestUser guestUser;

    @Mock
    private Visit visit;

    @Mock
    private SchedulingMessage schedulingMessage;

    @Before
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        service = spy(new VisitSchedulingService(userService, calendarService));
        
        doReturn(schedulingMessage).when(service).schedulingMessage(anyBoolean(), any());
    }

    @Test
    public void testAdd() throws Exception {
        boolean hasPermissions = true;
        doReturn(hasPermissions).when(service).userHasCreatePermissions(basicUser);

        assertTrue(service.add(basicUser, visit) instanceof SchedulingMessage);
        verify(service, times(1)).schedulingMessage(eq(hasPermissions), any());
    }

    @Test
    public void testAdd_butNoPermissions() throws Exception {
        boolean hasPermissions = false;
        doReturn(hasPermissions).when(service).userHasCreatePermissions(guestUser);

        assertTrue(service.add(guestUser, visit) instanceof SchedulingMessage);
        verify(service, times(1)).schedulingMessage(eq(hasPermissions), any());
    }

    @Test
    public void testEdit() throws Exception {
        boolean hasPermissions = true;
        doReturn(hasPermissions).when(service).userHasEditPermissions(admin);

        assertTrue(service.edit(admin, KEY, visit) instanceof SchedulingMessage);
        verify(service, times(1)).schedulingMessage(eq(hasPermissions), any());
    }

    @Test
    public void testEdit_butNoPermissions() throws Exception {
        boolean hasPermissions = false;
        doReturn(hasPermissions).when(service).userHasEditPermissions(guestUser);

        assertTrue(service.edit(guestUser, KEY, visit) instanceof SchedulingMessage);
        verify(service, times(1)).schedulingMessage(eq(hasPermissions), any());
    }

    @Test
    public void testRemove() throws Exception {
        boolean hasPermissions = true;
        doReturn(hasPermissions).when(service).userHasRemovePermissions(basicUser);

        assertTrue(service.remove(basicUser, KEY) instanceof SchedulingMessage);
        verify(service, times(1)).schedulingMessage(eq(hasPermissions), any());
    }

    @Test
    public void testRemove_butNoPermissions() throws Exception {
        boolean hasPermissions = false;
        doReturn(hasPermissions).when(service).userHasRemovePermissions(guestUser);

        assertTrue(service.remove(guestUser, KEY) instanceof SchedulingMessage);
        verify(service, times(1)).schedulingMessage(eq(hasPermissions), any());
    }


    @Test
    public void testGet() throws Exception {
        assertTrue(service.get(KEY) instanceof SchedulingMessage);
    }
}
